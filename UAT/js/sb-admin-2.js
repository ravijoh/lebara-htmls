
$(function() {

    var addDomLoadClass = function(){
        var topOffset = 50;
        
        var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
        console.log(width);
        if (width < 992) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu

            $('.containerHolder').removeClass('container');
            $('#side-menu > li:nth-child(3n+0)').addClass('right');
            $('#side-menu > li:nth-child(3n+1)').addClass('left');
            $('#side-menu > li:nth-child(3n+2)').addClass('middle');
            var winWidth = $( window ).width();
        } else {
            console.log(' Hi 4');
            $('div.navbar-collapse').removeClass('collapse');
            $('.containerHolder').addClass('container');
        }

        var height = (window.innerHeight > 0) ? window.innerHeight : screen.height;


        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    };


    $('#side-menu').metisMenu();
    $('.shipping-name-panel-event-up').click(function(){
        //$(this).slideUp();
        $('.new-address-popup').animate({height: "toggle",opacity: "0",width: ["toggle","swing"]});
    });
    $('.shipping-name-panel-event-down').click(function(){
        //$('.shipping-name-panel-event-up').slideDown();
        $('.new-address-popup').animate({height: "toggle",opacity: "1",width: ["toggle","swing"]});
    });

    $('.btn-cancelpassword').click(function(){
        //$(this).slideUp();
        $('.collapseChangePassword').animate({height: "toggle",opacity: "0",width: ["toggle","swing"]});
        $('.btn-savepassword, .btn-cancelpassword').hide();
        $('.btn-changepassword, .btn-editprofile').show();
    });
    $('.btn-changepassword').click(function(){
        //$('.shipping-name-panel-event-up').slideDown();
        $('.collapseChangePassword').animate({height: "toggle",opacity: "1",width: ["toggle","swing"]});
        $('.btn-changepassword, .btn-editprofile').hide();
        $('.btn-savepassword, .btn-cancelpassword').show();
    });


    //**************************Top UP form1******************************//
    $('.enter-voucher-panel-event-up').click(function(){
        //$(this).slideUp();
        $('.enter-voucher-panel-event-up, .printed-epin-panel, .pick-epin-panel, .orBadge').animate({height: "toggle",opacity: "0",width: ["toggle","swing"]});
    });
    $('.enter-voucher-panel-event-down').click(function(){
        //$('.shipping-name-panel-event-up').slideDown();
        $('.enter-voucher-panel-event-up, .printed-epin-panel, .pick-epin-panel, .orBadge').animate({height: "toggle",opacity: "1",width: ["toggle","swing"]});
    });

    //**************************Top UP form2******************************//
    $('.enter-epin-panel-event-up').click(function(){
        //$(this).slideUp();
        $('.enter-epin-panel-event-up, .physical-voucher-panel, .pick-epin-panel, .orBadge').animate({height: "toggle",opacity: "0",width: ["toggle","swing"]});
    });
    $('.enter-epin-panel-event-down').click(function(){
        //$('.shipping-name-panel-event-up').slideDown();
        $('.enter-epin-panel-event-up, .physical-voucher-panel, .pick-epin-panel, .orBadge').animate({height: "toggle",opacity: "1",width: ["toggle","swing"]});
    });

        //**************************Top UP form3******************************//
    $('.pick-epin-panel-event-up').click(function(){
        //$(this).slideUp();
        $('.pick-epin-panel-event-up, .physical-voucher-panel, .printed-epin-panel, .orBadge').animate({height: "toggle",opacity: "0",width: ["toggle","swing"]});
    });
    $('.pick-epin-panel-event-down').click(function(){
        //$('.shipping-name-panel-event-up').slideDown();
        $('.pick-epin-panel-event-up, .physical-voucher-panel, .printed-epin-panel, .orBadge').animate({height: "toggle",opacity: "1",width: ["toggle","swing"]});
    });

    addDomLoadClass();

    //Loads the correct sidebar on window load,
    //collapses the sidebar on window resize.
    // Sets the min-height of #page-wrapper to window size
    
    $(window).on("resize", function() {      
        addDomLoadClass();
    });

    //Sidebar navigation ie9 bug resove
    if ($('html').hasClass('no-cssanimations')) {
        $('.sidebar ul li').click(function () {
            $(this).find('.nav-second-level').addClass('in');
            if ($(this).hasClass('active') === false) {
                $(this).find('.nav-second-level').removeClass('in');
                $(this).find('.nav-second-level').addClass('collapse');
            }
        });
    }
});